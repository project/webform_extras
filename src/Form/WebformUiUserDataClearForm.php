<?php

namespace Drupal\webform_extras\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformUiUserDataClearForm.
 */
class WebformUiUserDataClearForm extends FormBase {

  /**
   * Drupal\webform_extras\WebformEstrasInterface definition.
   *
   * @var Drupal\webform_extras\WebformEstrasInterface
   */
  protected $webformExtras;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->webformExtras = $container->get('webform_extras.default');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_ui_user_data_clear_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Webform UI user data'),
      '#title' => $this->t('Clear Webform UI user data'),
      '#weight' => '0',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->webformExtras->clearWebformUiUserData();
    $this->messenger()->addMessage('Webform UI user data has been cleared!');
  }

}
