<?php

namespace Drupal\webform_extras;

use Drupal\user\UserDataInterface;

/**
 * Class WebformExtrasService.
 */
class WebformExtrasService implements WebformEstrasInterface {

  /**
   * Drupal\user\UserDataInterface definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Constructs a new WebformExtrasService object.
   */
  public function __construct(UserDataInterface $user_data) {
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public function clearWebformUiUserData() {
    $this->userData->delete('webform_ui', NULL, 'element_type_preview');
  }

}
