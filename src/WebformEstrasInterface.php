<?php

namespace Drupal\webform_extras;

/**
 * Interface WebformEstrasInterface.
 */
interface WebformEstrasInterface {

  /**
   * Helps to clear the webform ui user data.
   */
  public function clearWebformUiUserData();

}
