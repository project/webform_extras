<?php

namespace Drupal\webform_extras\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush command to clear the webform ui user data.
 */
class ClearWebformUiUserDataCommands extends DrushCommands {

  /**
   * Drush command to clear the webform ui user data.
   *
   * @command clear_webformui_user_data
   * @aliases clear_wfuidata
   * @usage clear_webformui_user_data
   */
  public function updateLayoutKeys() {
    $webform_extras = \Drupal::service('webform_extras.default')->clearWebformUiUserData();
    $this->output()->writeln('Webform UI user data has been cleared!');
  }

}
